<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home(){
        $title = 'WELCOME TO GOLDEN BELLS CALENDAR';
       return view ('pages.home')->with('title', $title); 
    }

    public function aboutus(){
        $title = 'BELIEFS';
        return view ('pages.aboutus')->with('title', $title); 
     }

     public function dailydevotion(){
        $title = 'Daily Readings';
        return view ('pages.dailydevotion')->with('title', $title); 
     }

     public function contactus(){
      $title = 'Contact Us';
      return view ('pages.contactus')->with('title', $title); 
        
     }

     public function subscribe(){
      $title = 'subscribe';
      return view ('pages.subscribe')->with('title', $title); 
   }
}




