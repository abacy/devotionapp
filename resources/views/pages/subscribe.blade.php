@extends('layouts.app')

@section('content')

      <div class="jumbotron">
        <h1 class="display-4">Subscribe</h1>
        <p class="lead">Register here for daily devotional readings.</p>
        <div class="form-group">
                {{Form::label('title' , 'NAME')}}
                {{Form::text('title', '', ['class' => 'form-control' , 'placeholder' => 'surname'])}}
              </div>   
              <hr class="my-4">
              <div class="form-group">
                      {{Form::label('other name' , 'OTHER NAME')}}
                      {{Form::text('title', '', ['class' => 'form-control' , 'placeholder' => 'other name'])}}
                    </div>
                    <hr class="my-4">
                    <div class="form-group">
                          {{Form::label('email', 'E-MAIL ADDRESS')}}
                          {{Form::text('email', '', ['class' => 'form-control' , 'placeholder' => 'e-mail '])}}
                        </div>
                        <hr class="my-4">
                        <div class="form-group">
                                {{Form::label('phone number', 'PHONE NUMBER')}}
                                {{Form::text('phone number', '', ['class' => 'form-control' , 'placeholder' => 'phone number '])}}
                              </div>
                              <hr class="my-4">
                        
          {!! Form::close() !!}
    
        <p>Thank You!.</p>
        <p class="lead">
          <a class="btn btn-primary btn-lg" href="#" role="button">Subscribe</a>
        </p>
      </div>
@endsection

