       @extends('layouts.app')

       @section('content')
       <div class="jumbotron jumbotron-fluid">
                     <div class="container">
                       <h1 class="display-4">Welcome to <br>GOLDEN BELLS<br></h1>
                       <h1 class="display-4">Calendar</h1>
                       <p class="lead">Get daily BIBLE devotions and enrich yourself with the GOD'S WORD.</p>
                       <hr class="my-4">
                       <p class="lead">
                      <a class="btn btn-primary btn-lg" href="subscribe" role="button">Subscribe</a>
                     </div>
                   </div>
      @endsection
