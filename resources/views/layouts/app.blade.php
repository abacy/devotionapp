<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <title>{{config('app.name','DEVOTIONAPP')}}</title>
        <style>
                * {
                  box-sizing: border-box;
                }
                
                body {
                  font-family: Arial, Helvetica, sans-serif;
                }
                
                /* Style the header */
                header {
                  background-color: #905;
                  padding: 30px;
                  text-align: left;
                  font-size: 35px;
                  color: white;
                  width: 70%;
                }
                
                
                article {
                  float: left;
                  padding: 20px;
                  width: 70%;
                  background-color: #f1f1f1;
                  height: 300px; /* only for demonstration, should be removed */
                }
                
            
                
                /* Responsive layout - makes the two columns/boxes stack on top of each other instead of next to each other, on small screens */
                @media (max-width: 600px) {
                  nav, article {
                    width: 100%;
                    height: auto;
                  }
                }
                </style>  
        
    </head>
    <body>
        
     @include('inc.navbar')   
    <div class="container">
       @yield('content')
    </div>
   
    
    
    
    
    </body>
</html>